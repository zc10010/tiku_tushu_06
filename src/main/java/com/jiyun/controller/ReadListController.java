package com.jiyun.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Book;
import com.jiyun.entity.ReadList;
import com.jiyun.entity.User;
import com.jiyun.entity.UserMsg;
import com.jiyun.service.IBookService;
import com.jiyun.service.IReadListService;
import com.jiyun.service.IUserService;
import com.jiyun.vo.ReadListVo;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
@RestController
@RequestMapping("/read-list")
public class ReadListController {

    @Autowired
    IReadListService readListService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addReadList(@RequestBody ReadList readList){
        readListService.save(readList);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateReadList(@RequestBody ReadList readList){
        readListService.updateById(readList);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        readListService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        readListService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public ReadList findById(@RequestParam Integer id){
        return readListService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<ReadList> findAll(){
        return readListService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<ReadListVo> findPage(@RequestParam Integer page, @RequestParam Integer pageSize, @RequestBody ReadList readList){
        return readListService.findPage(page,pageSize,readList);
    }

    @Autowired
    IBookService bookService;

    @RequestMapping("backBook")
    public void backBook(Integer id){
        /**
         * 修改借阅记录
         * 修改书的状态
         */

        ReadList readList = readListService.getById(id);
        readList.setBackTime(new Date());
        readListService.updateById(readList);


        Book book = new Book();
        book.setId(readList.getBid());
        book.setState("未借阅");

        bookService.updateById(book);


    }

    @Autowired
    IUserService userService;

    @Autowired
    AmqpTemplate amqpTemplate;

    @RequestMapping("cuiBackBook")
    public void cuiBackBook(Integer id){

        /**
         * 查出记录
         * 查出人
         * 查出书
         * 组装消息
         */

        ReadList readList = readListService.getById(id);
        Book book = bookService.getById(readList.getBid());
        User user = userService.getById(readList.getUid());


        HashMap hashMap = new HashMap();
        hashMap.put("uid",user.getId());
        hashMap.put("msg",user.getUsername()+" 你好，你借的书《"+book.getName()+"》管理员催你还书了");

        amqpTemplate.convertAndSend("mymsg", JSONObject.toJSONString(hashMap));//发送点对点消息

    }





}
