package com.jiyun.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.UserMsg;
import com.jiyun.service.IUserMsgService;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
@RestController
@RequestMapping("/user-msg")
public class UserMsgController {

    @Autowired
    IUserMsgService userMsgService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addUserMsg(@RequestBody UserMsg userMsg){
        userMsgService.save(userMsg);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateUserMsg(@RequestBody UserMsg userMsg){
        userMsgService.updateById(userMsg);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        userMsgService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        userMsgService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public UserMsg findById(@RequestParam Integer id){
        return userMsgService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<UserMsg> findAll(){
        return userMsgService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<UserMsg> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<UserMsg> userMsgPage = new Page<>(page, pageSize);
        return userMsgService.page(userMsgPage);
    }

    @RabbitListener(exclusive = true,queuesToDeclare = @Queue("mymsg"))
    public void onMessage1(String msg){
        System.out.println("simple1模式接收到消息："+msg);

        Map map = JSONObject.parseObject(msg, Map.class);


        UserMsg userMsg = new UserMsg();

        userMsg.setUid(Integer.parseInt(map.get("uid").toString()));
        userMsg.setMsg(map.get("msg").toString());
        userMsg.setCreateTime(new Date());

        userMsg.setState("未读");

        userMsgService.save(userMsg);






    }

    @RequestMapping("findMyUnreadMsg")
    public List<UserMsg> findMyUnreadMsg(Integer uid){
        LambdaQueryWrapper<UserMsg> userMsgLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userMsgLambdaQueryWrapper.eq(UserMsg::getUid,uid);
        userMsgLambdaQueryWrapper.eq(UserMsg::getState,"未读");

        return userMsgService.list(userMsgLambdaQueryWrapper);

    }



}
