package com.jiyun.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.User;
import com.jiyun.pojo.MyResult;
import com.jiyun.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addUser(@RequestBody User user){
        userService.save(user);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateUser(@RequestBody User user){
        userService.updateById(user);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        userService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        userService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public User findById(@RequestParam Integer id){
        return userService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<User> findAll(){
        return userService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<User> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<User> userPage = new Page<>(page, pageSize);
        return userService.page(userPage);
    }

    @RequestMapping("login")
    public MyResult login(@RequestBody User user){

        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(User::getUsername,user.getUsername());
        User userDB = userService.getOne(userLambdaQueryWrapper);

        if (userDB==null) {
            return MyResult.ERROR("账号不存在");
        }

        if (!userDB.getPassword().equals(user.getPassword())) {
            return MyResult.ERROR("密码错误");
        }

        return MyResult.OK(userDB);


    }

}
