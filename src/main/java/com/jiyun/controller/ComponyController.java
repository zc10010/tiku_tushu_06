package com.jiyun.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.jiyun.entity.Compony;
import com.jiyun.service.IComponyService;
import java.util.List;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
@RestController
@RequestMapping("/compony")
public class ComponyController {

    @Autowired
    IComponyService componyService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addCompony(@RequestBody Compony compony){
        componyService.save(compony);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateCompony(@RequestBody Compony compony){
        componyService.updateById(compony);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        componyService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        componyService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public Compony findById(@RequestParam Integer id){
        return componyService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<Compony> findAll(){
        return componyService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<Compony> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<Compony> componyPage = new Page<>(page, pageSize);
        return componyService.page(componyPage);
    }

}
