package com.jiyun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.ReadList;
import com.jiyun.vo.ReadListVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
public interface ReadListMapper extends BaseMapper<ReadList> {

    Page<ReadListVo> findPage(Page<ReadListVo> readListVoPage,@Param("r") ReadList readList);
}
