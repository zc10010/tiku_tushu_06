package com.jiyun.mapper;

import com.jiyun.entity.UserMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
public interface UserMsgMapper extends BaseMapper<UserMsg> {

}
