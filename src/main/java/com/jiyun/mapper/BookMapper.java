package com.jiyun.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Book;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiyun.vo.BookVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
public interface BookMapper extends BaseMapper<Book> {

    Page<BookVo> findPage(Page<BookVo> bookVoPage, @Param("b") Book book);
}
