package com.jiyun.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class MyResult implements Serializable {

    private Integer code;//0成功；1失败
    private String msg;//错误信息
    private Object data;//数据


    public static MyResult OK(){
        MyResult myResult = new MyResult();
        myResult.setCode(0);
        return myResult;
    }

    public static MyResult OK(Object data){
        MyResult myResult = new MyResult();
        myResult.setCode(0);
        myResult.setData(data);
        return myResult;
    }

    public static MyResult ERROR(String msg){
        MyResult myResult = new MyResult();
        myResult.setCode(1);
        myResult.setMsg(msg);
        return myResult;
    }


}
