package com.jiyun.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.ReadList;
import com.jiyun.mapper.ReadListMapper;
import com.jiyun.service.IReadListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiyun.vo.ReadListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
@Service
public class ReadListServiceImpl extends ServiceImpl<ReadListMapper, ReadList> implements IReadListService {


    @Autowired
    ReadListMapper readListMapper;
    @Override
    public Page<ReadListVo> findPage(Integer page, Integer pageSize, ReadList readList) {
        Page<ReadListVo> readListVoPage = new Page<>(page, pageSize);
        readListMapper.findPage(readListVoPage,readList);
        return readListVoPage;
    }
}
