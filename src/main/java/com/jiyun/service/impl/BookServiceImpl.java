package com.jiyun.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Book;
import com.jiyun.mapper.BookMapper;
import com.jiyun.service.IBookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiyun.vo.BookVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements IBookService {

    @Autowired
    BookMapper bookMapper;

    @Override
    public Page<BookVo> findPage(Integer page, Integer pageSize, Book book) {

        Page<BookVo> bookVoPage = new Page<>(page, pageSize);

        bookMapper.findPage(bookVoPage,book);

        return bookVoPage;
    }
}
