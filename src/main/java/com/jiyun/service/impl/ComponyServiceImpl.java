package com.jiyun.service.impl;

import com.jiyun.entity.Compony;
import com.jiyun.mapper.ComponyMapper;
import com.jiyun.service.IComponyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
@Service
public class ComponyServiceImpl extends ServiceImpl<ComponyMapper, Compony> implements IComponyService {

}
