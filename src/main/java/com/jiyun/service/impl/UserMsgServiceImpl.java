package com.jiyun.service.impl;

import com.jiyun.entity.UserMsg;
import com.jiyun.mapper.UserMsgMapper;
import com.jiyun.service.IUserMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
@Service
public class UserMsgServiceImpl extends ServiceImpl<UserMsgMapper, UserMsg> implements IUserMsgService {

}
