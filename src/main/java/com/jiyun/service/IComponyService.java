package com.jiyun.service;

import com.jiyun.entity.Compony;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
public interface IComponyService extends IService<Compony> {

}
