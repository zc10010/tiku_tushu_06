package com.jiyun.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.ReadList;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jiyun.vo.ReadListVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
public interface IReadListService extends IService<ReadList> {

    Page<ReadListVo> findPage(Integer page, Integer pageSize, ReadList readList);
}
