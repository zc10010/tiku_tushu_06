package com.jiyun.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Book;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jiyun.vo.BookVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-16
 */
public interface IBookService extends IService<Book> {

    Page<BookVo> findPage(Integer page, Integer pageSize, Book book);
}
