/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50561
Source Host           : localhost:3306
Source Database       : tiku_tushu_06

Target Server Type    : MYSQL
Target Server Version : 50561
File Encoding         : 65001

Date: 2023-05-16 15:03:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `nums` int(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES ('1', '三国演义', '张三', '1', '12', '未借阅');
INSERT INTO `book` VALUES ('2', '红楼梦', '李四', '3', '1', '借阅中');
INSERT INTO `book` VALUES ('4', '西游四', '赵六', '2', '10', '未借阅');
INSERT INTO `book` VALUES ('5', '二十四史', '田七', '2', '1', '未借阅');

-- ----------------------------
-- Table structure for compony
-- ----------------------------
DROP TABLE IF EXISTS `compony`;
CREATE TABLE `compony` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of compony
-- ----------------------------
INSERT INTO `compony` VALUES ('1', '积云出版社');
INSERT INTO `compony` VALUES ('2', '优思安出版社');
INSERT INTO `compony` VALUES ('3', '金积云出版社');

-- ----------------------------
-- Table structure for read_list
-- ----------------------------
DROP TABLE IF EXISTS `read_list`;
CREATE TABLE `read_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bid` int(11) DEFAULT NULL,
  `read_time` datetime DEFAULT NULL,
  `back_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of read_list
-- ----------------------------
INSERT INTO `read_list` VALUES ('4', '2', '1', '2023-05-16 14:08:24', '2023-05-16 14:08:38');
INSERT INTO `read_list` VALUES ('5', '2', '2', '2023-05-16 14:08:26', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '1', '111', '管理员');
INSERT INTO `user` VALUES ('2', '2', '2', '222', '普通用户');
INSERT INTO `user` VALUES ('3', '3', '3', '333', '普通用户');

-- ----------------------------
-- Table structure for user_msg
-- ----------------------------
DROP TABLE IF EXISTS `user_msg`;
CREATE TABLE `user_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_msg
-- ----------------------------
INSERT INTO `user_msg` VALUES ('1', '2', '2 你好，你接的书《红楼梦》管理员催你还书了', '2023-05-16 14:24:34', '已读');
INSERT INTO `user_msg` VALUES ('2', '2', '2 你好，你借的书《红楼梦》管理员催你还书了', '2023-05-16 14:35:57', '已读');
INSERT INTO `user_msg` VALUES ('3', '2', '2 你好，你借的书《红楼梦》管理员催你还书了', '2023-05-16 14:36:08', '已读');
INSERT INTO `user_msg` VALUES ('4', '2', '2 你好，你借的书《红楼梦》管理员催你还书了', '2023-05-16 14:36:08', '已读');
